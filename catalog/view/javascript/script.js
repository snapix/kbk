$(document).ready(function () {
    let big_block = $('.subcategories_list .big-block'),
        mini_block = $('.subcategories_list .mini-block');
    $('.subcategories_list .arrow').click(function () {
       if ($(this).hasClass('active')){
           $(this).removeClass('active');
           big_block.hide(300);
           mini_block.show(300);
       }else{
           $(this).addClass('active');
           big_block.show(300);
           mini_block.hide(300);
       }
    })
});