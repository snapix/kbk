document.addEventListener("DOMContentLoaded", function () {
    function map(block_id, x, y){
        ymaps.ready(function () {
            var myMap = new ymaps.Map(block_id, {
                center: [x,y],
                zoom: 17,
                controls: ['zoomControl', 'fullscreenControl', 'rulerControl'],
                scroll: false,
            });

            myGeoObjects = new ymaps.Placemark([x,y]);
            myMap.geoObjects.add(new ymaps.Placemark([x,y], {
                balloonContent: 'КирпичБлокКровля'
            }, {
                preset: 'islands#yellowHomeIcon'
            }));
        });
    }

    ymaps.ready(map('map-kolomna', 55.0814260696186, 38.759717499999944));
    ymaps.ready(map('map-moscow', 55.39484156932795,37.431701999999895));
});






