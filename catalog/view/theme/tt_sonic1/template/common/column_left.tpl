<?php if ($modules) { ?>
<aside id="column-left" class="col-sm-3">
  <!--вывод главных категорий над фильтром-->
  <?php if (isset($mainCategories) and is_array($mainCategories) and !empty($mainCategories) ) { ?>
    <div class="left-menu-categories">
      <p class="category-name"><b><?php echo $category_name; ?></b></p>
      <div class="links">
        <?php foreach ($mainCategories as $key => $category){ ?>
        <a href="<?= $category['href'] ?>"><?= $category['name'] ?></a><br>
        <?php }; ?>
      </div>
    </div>
  <?php }; ?>
  <!--вывод главных категорий над фильтром-->

  <?php foreach ($modules as $module) { ?>
  <?php echo $module; ?>
  <?php } ?>
</aside>
<?php } ?>
