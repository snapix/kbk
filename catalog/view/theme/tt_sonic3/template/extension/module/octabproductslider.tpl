<?php $tab_effect = 'wiggle'; ?>
<script type="text/javascript">
$(document).ready(function() {

	$(".tab_content").hide();
	$(".tab_content:first").show(); 

	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active");
		$(this).addClass("active");
		$(".tab_content").hide();
		$(".tab_content").removeClass("animate1 <?php echo $tab_effect;?>");
		var activeTab = $(this).attr("rel"); 
		$("#"+activeTab) .addClass("animate1 <?php echo $tab_effect;?>");
		$("#"+activeTab).fadeIn(); 
	});
});

</script>
<div class="product-tabs-container-slider">
	<div class="title-product-tabs">
		<div class="containers">
			<ul class="tabs"> 
			<?php $count=0; ?>
			<?php foreach($productTabslider as $productTab ){ ?>
				<li rel="tab_<?php echo $productTab['id']; ?>"  >
					<h3 class="tab_<?php echo $productTab['id']; ?>"><?php echo $productTab['name']; ?></h3>
				</li>
					<?php $count= $count+1; ?>
			<?php } ?>	
			</ul>
		</div>
	</div>
	<div class="tab_container">
		<div class="containers">
		<?php foreach($productTabslider as $productTab){ ?>
			<div id="tab_<?php echo $productTab['id']; ?>" class="tab_content">
				<div class="row-demo-tabproduct">
				<div class="owl-demo-tabproduct">
				<?php $i=0; $row=1; $i1=0; ?>
				<?php foreach ($productTab['productInfo'] as $product){ ?>
					<div class="item">
						<div class="item-inner">
							<div class="oc-box-content">
								<div class="products product-thumb">
									<div class="product-img-content">
										<a class="product-img" href="<?php echo $product['href']; ?>">
											<div class="product-image">
												<?php if($product['rotator_image']): ?>
													<img class="img2" src="<?php echo $product['rotator_image']; ?>" alt="<?php echo $product['name']; ?>" />
												<?php endif; ?>
												<img class="img1" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
											</div>
										</a>
										<?php if($config_slide['f_show_addtocart']) { ?>
										<div class="add-to-links">
											<button class="btn-wishlist" type="button"  title="<?php echo $button_wishlist; ?>"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
												<span class="effect-btn effect-wishlist">
													<i class="fa fa-heart-o"></i>
												</span>
												<em class="tooltips"><?php echo $tooltip_wishlist; ?></em>
											</button>
											<button class="btn-compare" type="button"  title="<?php echo $button_compare; ?>"  onclick="compare.add('<?php echo $product['product_id']; ?>');">
												<span class="effect-btn effect-compare">
													<i class="fa fa-refresh"></i>
												</span>
												<em class="tooltips"><?php echo $tooltip_compare; ?></em>
											</button>
										</div> <!-- end-add-to-link -->
										<?php } ?>
										<?php if ($product['is_new']):
											if($product['special']): ?>
												<div class="label-pro-sale"><?php echo $text_sale; ?></div>
											<?php else: ?>
												<div class="label-pro-new"><?php echo $text_new; ?></div>
											<?php endif; ?>
										<?php else: ?>
											<?php if($product['special']): ?>
												<div class="label-pro-sale"><?php echo $text_sale; ?></div>
											<?php endif; ?>
										<?php endif; ?>
									</div><!-- product-img-content -->
									<?php if($config_slide['f_show_des']) { ?>
										<div class="des"><?php echo $product['description']; ?></div>
									<?php } ?>
									<div class="top-inner">
										<h2 class="product-name">
											<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
										</h2>
										<?php if (isset($product['rating'])) { ?>
											<div class="rating"><img src="catalog/view/theme/tt_sonic1/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
										<?php } ?>
										<?php if($config_slide['f_show_price']) { ?>
											<?php if ($product['price']) { ?>
											<div class="price-box">
												<?php if (!$product['special']) { ?>
													<span class="price"><?php echo $product['price']; ?></span>
												<?php } else { ?>
													<p class="old-price"><span class="price"><?php echo $product['price']; ?></span></p>
													<p class="special-price"><span class="price"><?php echo $product['special']; ?></span></p>
												<?php } ?>
											</div>
											<?php } ?>
										<?php } ?>
										
									</div><!-- top-inner -->
									<div class="actions">
										<?php if($config_slide['f_show_addtocart']) { ?>
											<button class="button btn-cart" type="button"  title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');">
												<i class="fa fa-plus"></i>
												<span class="effect-add-cart" data-hover="<?php echo $button_cart; ?>">
													<?php echo $button_cart; ?>
												</span>
											</button>
										<?php } ?>
									</div><!-- actions -->
								</div><!-- products -->
							</div><!-- oc-box-content -->
						</div> <!-- item-inner -->
					</div> <!-- item -->
				<?php } ?>
				</div>
				</div><!-- .row -->
			</div>

		<?php } ?>
	
		</div>
	</div><!-- .tab_container -->
</div>
<script type="text/javascript">
$(document).ready(function() { 
 $(".product-tabs-container-slider .tabs li:first").addClass("active");
  $(".owl-demo-tabproduct").owlCarousel({
		items : <?php if($config_slide['items']) { echo $config_slide['items'] ;} else { echo 3;} ?>,
		autoPlay : <?php if($config_slide['autoplay']) { echo 'true' ;} else { echo 'false';} ?>,
		slideSpeed: <?php if($config_slide['f_speed']) { echo $config_slide['f_speed'] ;} else { echo 3000;} ?>,
		navigation : <?php if($config_slide['f_show_nextback']) { echo 'true' ;} else { echo 'false';} ?>,
		paginationNumbers : true,
		pagination : <?php if($config_slide['f_show_ctr']) { echo 'true' ;} else { echo 'false';} ?>,
		stopOnHover : false,
		itemsDesktop : [1199,3], 
		itemsDesktopSmall : [991,3], 
		itemsTablet: [767,2], 
		itemsMobile : [479,1]
  });
 
});
</script>



