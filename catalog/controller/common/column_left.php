<?php
class ControllerCommonColumnLeft extends Controller {
	public function index() {
		$this->load->model('design/layout');

		if (isset($this->request->get['route'])) {
			$route = (string)$this->request->get['route'];
		} else {
			$route = 'common/home';
		}

		$layout_id = 0;

		if ($route == 'product/category' && isset($this->request->get['path'])) {
			$this->load->model('catalog/category');

			$path = explode('_', (string)$this->request->get['path']);
			
			// вывод главных категорий над фильтром
            $category_name = $this->model_catalog_category->getCategory(end($path));
            $data['category_name'] = $category_name['name'];

			$data['mainCategories'] = array();
			$results = $this->model_catalog_category->getCategories(end($path));
			foreach ($results as $result) {
				if (isset($result['main_category']) and $result['main_category']) {
					$params = isset($result['params']) ? $result['params'] : '';
					$data['mainCategories'][] = [
						'name' => $result['name'],
						'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'].'_'. $result['category_id']. $params),
						// TODO РАСКОМЕНТИТЬ и прописать название поля сортировки
						'sort' => $result['sort']
					];
				};
			};
//			var_dump($data['mainCategories']);
//            var_dump($path);
			// TODO РАСКОМЕНТИТЬ СОРТИРОВКУ КАТЕГОРИЙ ПОСЛЕ ДОБАВЛЕНИЯ СООТВЕТСТВУЮЩЕГО ПОЛЯ
            $sorts = array_column($data['mainCategories'],'sort');
            $names = array_column($data['mainCategories'],'name');
            array_multisort($sorts, SORT_ASC, $names, SORT_ASC, $data['mainCategories']);
			// вывод главных категорий над фильтром

			$layout_id = $this->model_catalog_category->getCategoryLayoutId(end($path));
		}

		if ($route == 'product/product' && isset($this->request->get['product_id'])) {
			$this->load->model('catalog/product');

			$layout_id = $this->model_catalog_product->getProductLayoutId($this->request->get['product_id']);
		}

		if ($route == 'information/information' && isset($this->request->get['information_id'])) {
			$this->load->model('catalog/information');

			$layout_id = $this->model_catalog_information->getInformationLayoutId($this->request->get['information_id']);
		}

		if (!$layout_id) {
			$layout_id = $this->model_design_layout->getLayout($route);
		}

		if (!$layout_id) {
			$layout_id = $this->config->get('config_layout_id');
		}

		$this->load->model('extension/module');

		$data['modules'] = array();

		$modules = $this->model_design_layout->getLayoutModules($layout_id, 'column_left');

		foreach ($modules as $module) {
			$part = explode('.', $module['code']);

			if (isset($part[0]) && $this->config->get($part[0] . '_status')) {
				$module_data = $this->load->controller('extension/module/' . $part[0]);

				if ($module_data) {
					$data['modules'][] = $module_data;
				}
			}

			if (isset($part[1])) {
				$setting_info = $this->model_extension_module->getModule($part[1]);

				if ($setting_info && $setting_info['status']) {
					$output = $this->load->controller('extension/module/' . $part[0], $setting_info);

					if ($output) {
						$data['modules'][] = $output;
					}
				}
			}
		}

		return $this->load->view('common/column_left', $data);
	}
}
